rem Add reg entry to disable Terminal services
reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 1 /f 
rem Reset and export firewall
netsh advfirewall reset export "c:\fireback.wfw"
rem Enable the domain profile
netsh advfirewall set domainprofile state on
rem Deny 3389/TCP
netsh advfirewall add rule name="Deny Remote Desktop" protocol=TCP dir=in localport=3389 action=deny
rem Log dropped connections
netsh advfirewall set domainprofile logging droppedconnections enable
